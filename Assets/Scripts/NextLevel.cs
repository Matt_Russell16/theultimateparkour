using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class NextLevel : MonoBehaviour
{

    public GameObject LevelTwo;
    public GameObject Cherries;


    private void Start()
    {
        Cherries.SetActive(true);
        LevelTwo.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        LevelTwo.SetActive(true);
        Cherries.SetActive(false);

        Time.timeScale = 0f;
    }
}