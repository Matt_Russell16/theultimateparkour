using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class FinishFlag : MonoBehaviour
{

    public GameObject FinishScreen;
    public GameObject Cherries;


    private void Start()
    {
        Cherries.SetActive(true);
        FinishScreen.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        FinishScreen.SetActive(true);
        Cherries.SetActive(false);

        Time.timeScale = 0f;
    }
}